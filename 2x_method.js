var baseBet = 10;                  // my baseBet
var myBits = 100000;                   // how many bits I have
var cashOut = 2;                  // cashOut rate
var looseMultiplier = 2;       

var gamesPlayed=0;              // number of games I played


engine.on('game_starting', function(info) {
   // engine.placeBet(Math.floor(currentBet / 100) * 100, Math.floor(cashOut * 100), false);
    if(gamesPlayed % 100 ==0 && gamesPlayed !=0){         // reporting the game status once per 200 games
        var report = "you have played "+gamesPlayed +" games\n";
        report += "your next bet: " + baseBet +"\n";
        report += "your bits left:  "+myBits + " bits\n";
        report += "your profit:  "+ 100000 - myBits + " bits\n";
        alert(report);
    }
    if(myBis<baseBet){                 // can not continue betting, ran out of bits, show report
        var report = "you have played "+gamesPlayed +" games\n";
        report += "your next bet: " + baseBet +"\n";
        report += "your bits left:  "+myBits + "m\n";
        var profit = 100000 - myBits;
        report += "your profit:  "+ profit + " bits\n";
        alert(report);
    }
    else{                             // you still have bits to bet with
        myBits -=baseBet;             // subtracting your bet from your bits
        // engine.placeBet(Math.floor(baseBet / 100) * 100, Math.floor(cashOut * 100), false);   places bet
    }
});

engine.on('game_crash', function(data) {
    var gameCrashed = data.game_crash;
    if(data.game_crash/100<2){       // lost the game
        baseBet*= looseMultiplier;
        gamesPlayed+=1;
    }else{                       // won the game
        myBits += baseBet;
        baseBet = 10;
        gamesPlayed+=1;
    } 
   
});